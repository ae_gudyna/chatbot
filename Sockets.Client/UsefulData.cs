﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sockets.Client
{
    public class UsefulData
    {
        public static readonly string[] Messages = 
        {
            "Hello", "I'm so tired today", "Hmmm, interesting...", "How are you?", "How was your day?",
            "Hola!", "Bon sour!", "Buy", "Good day!", "Salut!",
            "Today is very good day"
        };
        public static readonly string[] Names =
        {
            "Susie",
            "Michael",
            "Rick",
            "Penelopa",
            "Alex",
            "Ben",
            "Lia",
            "Pandora"
        };
    }
}
