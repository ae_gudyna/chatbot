﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Sockets.Client
{
    public class ChatBot
    {
        private static Random random = new Random();
        private static int randomSleepTime => random.Next(2000, 10000);
        private static int randomMessage => random.Next(0, UsefulData.Messages.Length - 1);

        public static void Run()
        {
            IPAddress ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];

            var countOfMessages = UsefulData.Messages.Length;
            foreach (var name in UsefulData.Names)
            {
                var nickName = name;
                new Thread(new ThreadStart(() => SendData(ipAddress, nickName))).Start();
                Thread.Sleep(randomSleepTime);
            }
        }

        private static void SendData(IPAddress ipAddress,string nickName)
        {
            var clientSocket = new ClientSocket(ipAddress, nickName);
            clientSocket.Connect();
            new Thread(new ThreadStart(() => clientSocket.Receive())).Start();
            for (var j = 0; j < randomMessage; j++)
            {
                Thread.Sleep(randomSleepTime);
                clientSocket.Send(UsefulData.Messages[randomMessage]);
                Thread.Sleep(randomSleepTime);
                clientSocket.Send(UsefulData.Messages[randomMessage]);
                Thread.Sleep(randomSleepTime);
            }
            clientSocket.CloseConnection();
        }

    }
}
