﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Sockets.Client
{
    public class ClientSocket
    {
        private readonly ManualResetEvent connectDone = new ManualResetEvent(false);
        private readonly ManualResetEvent sendDone = new ManualResetEvent(false);
        private readonly ManualResetEvent receiveDone = new ManualResetEvent(false);
        private readonly Socket client;
        private readonly IPEndPoint ipe;
        private readonly string socketName;
        private StreamWriter file;
        public bool Active { get; private set; } = true;

        public ClientSocket(IPAddress ipAddress, string socketName)
        {
            client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            ipe = new IPEndPoint(ipAddress, 11000);
            this.socketName = socketName;
        }
        public void Connect()
        {
            client.BeginConnect(ipe, new AsyncCallback(ConnectCallback), client);

            connectDone.WaitOne();
            Directory.CreateDirectory(@"C:\Users\Public\ChatBot");
            file = new StreamWriter($@"C:\Users\Public\ChatBot\{socketName}.txt", true);
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            Socket client = (Socket)ar.AsyncState;
            client.EndConnect(ar);

            Console.WriteLine($"Socket {socketName} connected to {client.RemoteEndPoint.ToString()}" );
            this.Send(socketName);

            connectDone.Set();
        }

        public void Send(string data)
        { 
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            
            client.BeginSend(byteData, 0, byteData.Length, SocketFlags.None,
                new AsyncCallback(SendCallback), client);
            sendDone.WaitOne();
        }

        private void SendCallback(IAsyncResult ar)
        {
            Socket client = (Socket)ar.AsyncState;
            int bytesSent = client.EndSend(ar);
            sendDone.Set();
        }

        public void Receive()
        {
            while (Active)
            {
                receiveDone.Reset();
                StateObject state = new StateObject();
                state.WorkSocket = client;

                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);

                receiveDone.WaitOne();
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            if (Active)
            {
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.WorkSocket;
                int bytesRead = client.EndReceive(ar);
                
                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));
                if (client.Available > 0)
                {
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    if (state.sb.Length > 0)
                    {
                        var response = state.sb.ToString();
                        file.WriteLine(response);
                    }
                    receiveDone.Set();
                }
            }
        }
        public void CloseConnection()
        {
            Console.WriteLine($"Closing connection {this.socketName}");
            Active = false;
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            file.Dispose();
        }
    }
}
