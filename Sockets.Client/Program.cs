﻿using System;

namespace Sockets.Client
{
    class Program
    {

        static void Main(string[] args)
        {
            ChatBot.Run();
            Console.ReadLine();
        }
    }
}
