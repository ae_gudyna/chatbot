﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Sockets.Server
{
    public class Listener
    {
        private ManualResetEvent allDone = new ManualResetEvent(false);
        private ManualResetEvent receiveDone = new ManualResetEvent(false);
        private Socket listener;
        private int backlog;
        private IPAddress ipAddress;
        private Story story;

        public delegate void NewMessageHandler(string message);

        public event NewMessageHandler SendNewMessage;
        public Listener(IPAddress ipAddress, int backlog = 10, int storySize = 5)
        {
            this.ipAddress = ipAddress;
            listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            this.backlog = backlog;
            story = new Story(storySize);
        }

        public void StartListen()
        {
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);
            listener.Bind(localEndPoint);
            listener.Listen(10);
            while (true)
            {
                BeginAccept();
            }
        }

        void BeginAccept()
        {
            allDone.Reset();
            Thread clientThread = new Thread(new ThreadStart(() => { listener.BeginAccept(new AsyncCallback(AcceptCallback), listener); }));
            clientThread.Start();
            allDone.WaitOne();
        }
        void AcceptCallback(IAsyncResult ar)
        {
            allDone.Set();

            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            var serverSocket = ServerSocket.Initialize(this, handler, story.GetStory());
            try
            {
                serverSocket.GetData();
            }
            catch(SocketException)
            {
                serverSocket.CloseConnection();
            }
        }
        public void NewMessage(string message)
        {
            SendNewMessage?.Invoke(message);
            story.AddMessage(message);
        }
    }
}
