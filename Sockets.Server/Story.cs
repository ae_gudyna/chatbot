﻿using System.Collections.Generic;

namespace Sockets.Server
{
    public class Story
    {
        private readonly Queue<string> queueOfMessages;
        private readonly int storySize;

        public Story(int storySize)
        {
            this.storySize = storySize;
            queueOfMessages = new Queue<string>(storySize);
        }

        public void AddMessage(string message)
        {
            if (queueOfMessages.Count > storySize)
            {
                queueOfMessages.Dequeue();
            }
            queueOfMessages.Enqueue(message);
        }

        public IEnumerable<string> GetStory()
        {
            return queueOfMessages;
        }
    }
}
