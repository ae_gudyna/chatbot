﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Sockets.Server
{
    public class ServerSocket
    {
        private string clientSocketName;
        private readonly Listener listener;
        private readonly Socket handler;
        private ManualResetEvent receiveDone = new ManualResetEvent(false);
        public bool Active { get; private set; } = true;

        private ServerSocket(Listener listener, Socket handler)
        {
            this.listener = listener;
            this.handler = handler;
        }
        public static ServerSocket Initialize(Listener listener, Socket handler, IEnumerable<string> storyCollection)
        {
            var serverSocket = new ServerSocket(listener, handler);
            serverSocket.GetName();
            listener.SendNewMessage += serverSocket.Send;

            var storyBuilder = new StringBuilder();
            foreach(var message in storyCollection)
            {
                storyBuilder.Append(message);
                storyBuilder.Append(Environment.NewLine);
            }
            serverSocket.Send(storyBuilder.ToString());

            return serverSocket;
        }
        private void GetName()
        {
            StateObject state = new StateObject();
            state.WorkSocket = handler;
            receiveDone.Reset();
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadNameCallback), state);
            receiveDone.WaitOne();
        }

        public void GetData()
        {
            StateObject state = new StateObject();
            state.WorkSocket = handler;
            while (Active)
            {
                receiveDone.Reset();
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                receiveDone.WaitOne();
            }
        }

        private void ReadNameCallback(IAsyncResult ar)
        {
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.WorkSocket;

            int read = handler.EndReceive(ar);

            state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, read));
            if (handler.Available > 0)
            {
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                   new AsyncCallback(ReadNameCallback), state);
            }
            else
            {
                if (state.sb.Length > 1)
                {
                    clientSocketName = state.sb.ToString();
                    var message = $"{clientSocketName} connected";
                    Console.WriteLine(message);
                    listener.NewMessage(message);
                    state.sb.Clear();
                }
                receiveDone.Set();
            }
        }
        private void ReadCallback(IAsyncResult ar)
        {
            if (Active)
            {
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.WorkSocket;
                int read;
                try
                {
                    read = handler.EndReceive(ar);
                }
                catch (SocketException)
                {
                    CloseConnection();
                    return;
                }

                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, read));
                if (handler.Available > 0)
                {
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                       new AsyncCallback(ReadCallback), state);
                }
                else
                {
                    if (state.sb.Length > 1)
                    {
                        var contentBuilder = new StringBuilder();
                        contentBuilder.Append(clientSocketName);
                        contentBuilder.Append(" (");
                        contentBuilder.Append(DateTimeOffset.Now.ToLocalTime().ToString("HH:mm:ss"));
                        contentBuilder.Append("): ");
                        contentBuilder.Append(state.sb);
                        contentBuilder.Append(Environment.NewLine);
                        var message = contentBuilder.ToString();
                        Console.WriteLine(message);
                        listener.NewMessage(message);
                        state.sb.Clear();
                    }
                    receiveDone.Set();
                }
            }
        }

        private void Send(string data)
        {
            if (Active)
            {
                byte[] byteData = Encoding.UTF8.GetBytes(data);
                handler.BeginSend(byteData, 0, byteData.Length, SocketFlags.None,
                    new AsyncCallback(SendCallback), handler);
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            Socket handler = (Socket)ar.AsyncState;
            int bytesSent = handler.EndSend(ar);
        }

        public void CloseConnection()
        {
            Active = false;
            listener.SendNewMessage -= Send;
            var content = clientSocketName + " disconnected";
            listener.NewMessage(content);
            Console.WriteLine(content);
        }
    }
}
