﻿using System;
using System.Net;
using System.Threading;

namespace Sockets.Server
{
    class Program
    {
        private static readonly int sizeOfMessageArray = 10;
        static void Main(string[] args)
        {
            Console.WriteLine("Open the listener...");
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            var listener = new Listener(ipAddress);
            listener.StartListen();
            Console.ReadLine();
        }


    }
}
