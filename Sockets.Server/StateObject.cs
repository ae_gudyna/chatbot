﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Sockets.Server
{
    public class StateObject
    {
        public Socket WorkSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
    }
}
